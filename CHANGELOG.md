# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.4.0

- minor: Add default values for AWS variables.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Update Readme with examples exclude-include files.

## 0.3.6

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.3.5

- patch: Updated the docs to mention the aws-cloudfront-invalidate pipe

## 0.3.4

- patch: Internal release to rewrite tests in python

## 0.3.3

- patch: Internal release

## 0.3.2

- patch: Refactor pipe code to use pipes bash toolkit.
- patch: Update aws-cli base docker image version.

## 0.3.1

- patch: Fix validation message if directory does'nt exist

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.4

- patch: Added contribution guidelines
- patch: Updated contributing guidelines

## 0.2.3

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.2.2

- patch: Standardising README and pipes.yml.

## 0.2.1

- patch: Fix README.md typo.

## 0.2.0

- minor: Allow debugging of the aws s3 command via the DEBUG variable.
- minor: Convert from tasks to pipes.

## 0.1.3

- patch: Use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Remove details

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines S3 deploy pipe.

